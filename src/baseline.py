from collections import namedtuple

Row = namedtuple('Row', "id feature1 feature2 time".split())


def read_data(path):
    """

    :param path: path to file with data
    :return: list of Row.
    """
    rows = []
    file = open(path)
    file.readline()
    for line in file:
        id, feature1, feature2, time = line.split(",")
        row = Row(int(id), int(feature1), int(feature2), float(time))
        rows.append(row)
    return rows


def is_predecessor(row1, row2, dt):
    """

    :param row1: target row
    :type row1: Row
    :param row2: possible predecessor
    :type row2: Row
    :param dt: max time interval
    :type dt: float
    :return: True if row2 is predecessor of row1, False otherwise
    """
    return (
        row1.feature1 == row2.feature1 and
        row1.feature2 == row2.feature2 and
        row1.time > row2.time and
        (row1.time - row2.time) < dt)


def do_the_job(path, dt):
    result = open("result_baseline.csv", "w")
    result.write("id,count\n")
    rows = read_data(path)

    for row1 in rows:
        n = 0
        for row2 in rows:
            if is_predecessor(row1, row2, dt):
                n += 1
        result.write("{},{}\n".format(row1.id, n))

if __name__ == "__main__":
    import sys
    path = sys.argv[1]
    dt = float(sys.argv[2])
    do_the_job(path, dt)

