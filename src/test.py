import os

import hashlib

import time

from baseline import do_the_job as baseline
from pandas_based import do_the_job as pd_based

print()

files = ["example.csv", "example2.csv", "example4.csv", "incidents_2_100.csv", "incidents_10_1000.csv", "example5.csv"]
path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data")
print(path)
files = [os.path.join(path, file) for file in files]

all_passed = True

for file in files:
    for dt in [0.01, 0.1, 0.5]:
        baseline(file, dt)
        pd_based(file, dt)
        hash_baseline = hashlib.md5(open('result_baseline.csv', 'rb').read()).hexdigest()
        hash_pd_based = hashlib.md5(open('result.csv', 'rb').read()).hexdigest()
        if hash_baseline == hash_pd_based:
            print(file, dt, " pass")
        else:
            print(file, dt, "FAIL!")
            all_passed = False
            os._exit(-1)

if all_passed:
    print("ALL PASS")


start = time.time()
pd_based(os.path.join(path, "incidents_100_1000000.csv.gz"), 0.3)
t = time.time() - start
print(t)

