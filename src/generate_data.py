import numpy as np
import pandas as pd
import sys


M = int(sys.argv[1])
N = int(sys.argv[2])

df = pd.DataFrame({'feature1': np.random.randint(M, size=(N,)),
               'feature2': np.random.randint(M, size=(N,)),
               'time': np.random.rand(N)
               })
df.to_csv('incidents_{}_{}.csv'.format(M, N), index_label='id')
