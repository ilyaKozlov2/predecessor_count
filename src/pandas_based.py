import time
import pandas as pd
import numpy as np
import sys


def is_predecessor(row1, row2, dt):
    """
    check if row2 is predecessor of row1. Here predecessor is not strict e.q. row is predecessor of itself

    :param row1: np.array [feature1, feature2, time]
    :type row1: np.ndarray
    :param row2: np.array [feature1, feature2, time]
    :type row2: np.ndarray
    :param dt: max time difference
    :type dt: float
    :return: True if row2 is predecessor of row1, False otherwise
    :rtype: bool
    """
    return row1[0] == row2[0] and row1[1] == row2[1] and 0 <= (row1[2] - row2[2]) < dt


def get_count(data, dt):
    """
    count number of predecessors for each incident in data. Incident B is a predecessor of the incident A if
        A.feature1 == B.feature1
        and A.feature2 == B.feature2
        and A.time > B.time
        and A.time - B.time > dt

    :param data: data frame with columns feature1(int 0.. M - 1), feature2 (int 0..M-1), time (float) and index column id
    :type data: pd.DataFrame

    :param dt: max time interval
    :type dt: float
    :return: data series with index column id and column count --- count of the predecessors
    :rtype: pd.Series
    """
    data_sort = data.sort_values(by=["feature1", "feature2", "time"])
    count = np.empty(data_sort.shape[0], dtype=int)

    predecessor_id = 0
    values = data_sort.values
    predecessor = values[predecessor_id]
    """
    rank and row_id not always the same thing. If two rows have same features and time they would have same rank
    but different row_id
    """
    rank = 0
    pre_time = None
    
    for row_id in range(values.shape[0]):
        row = values[row_id]
        if pre_time != row[2]:
            rank = row_id
        pre_time = row[2]

        while not is_predecessor(row, predecessor, dt):
            predecessor_id += 1
            predecessor = values[predecessor_id]
        count[row_id] = rank - predecessor_id

    data_sort["count"] = count
    data_sort.sort_index(inplace=True)
    return data_sort["count"]


def do_the_job(path, df):
    """
    read dataframe from file. Count number of predecessors for each incident and write result in the result file.

    :param path: path to file with data
    :type path: str
    :param df: max time interval
    :type df: float
    """
    start = time.time()
    data = pd.read_csv(path, index_col="id",
                       dtype={"feature1": np.int, "feature2": np.int, "time": np.float}, engine="c")
    print("time to read {}".format(time.time() - start))

    start = time.time()
    res = get_count(data, df)
    print("time to count {}".format(time.time() - start))

    start = time.time()
    res.to_csv("result.csv", header=True)
    print("time to write {}".format(time.time() - start))

if __name__ == "__main__":
    dt = float(sys.argv[2])
    path = sys.argv[1]
    do_the_job(path, dt)
